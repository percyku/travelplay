package com.example.percyku.play.travel.travelplay;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class register extends AppCompatActivity {

    Button btGirl , btBoy , btFinishRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        findview();
        btBoy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btBoy.setBackgroundResource(R.drawable.register_boygray);
                btGirl.setBackgroundResource(R.drawable.register_girl);
            }
        });
        btGirl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btBoy.setBackgroundResource(R.drawable.register_boy);
                btGirl.setBackgroundResource(R.drawable.register_girlgray);
            }
        });
        btFinishRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent itFinishRegister = new Intent();
                itFinishRegister.setClass(register.this, Login.class);
                startActivity(itFinishRegister);
            }
        });
        initial();
    }

    private void initial(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.register_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    public  void findview(){
        btGirl = (Button) this.findViewById(R.id.btGirl);
        btBoy = (Button) this.findViewById(R.id.btBoy);
        btFinishRegister = (Button) this.findViewById(R.id.btFinishRegister);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_register, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
