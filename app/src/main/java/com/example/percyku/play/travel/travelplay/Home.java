package com.example.percyku.play.travel.travelplay;

import android.content.Intent;
import android.os.PersistableBundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

public class Home extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener  {
    private Toolbar toolBar;
    private NavigationView navDrawer;
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle drawerToggle;
    private int selectedItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        toolBar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolBar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolBar.setOnMenuItemClickListener(onMenuItemClick);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        navDrawer = (NavigationView) findViewById(R.id.menu_drawer);
        navDrawer.setNavigationItemSelectedListener(this);
        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolBar, R.string.drawer_open, R.string.drawer_close);

        drawerLayout.setDrawerListener(drawerToggle);
        drawerToggle.syncState();

        selectedItem = savedInstanceState == null ? R.id.nav_item_1 : savedInstanceState.getInt("selectedItem");
    }

    private Toolbar.OnMenuItemClickListener onMenuItemClick = new Toolbar.OnMenuItemClickListener() {
        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            String msg = "";
            switch (menuItem.getItemId()) {
                case R.id.check_information:
                    msg += "Click check_information";

                    break;
            }

            if(!msg.equals("")) {
                Toast.makeText(Home.this, msg, Toast.LENGTH_SHORT).show();
            }
            return true;
        }
    };



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);

        outState.putInt("selectedItem", selectedItem);
    }


    @Override
    public boolean onNavigationItemSelected(MenuItem menuItem) {
        menuItem.setChecked(true);
        selectedItem = menuItem.getItemId();

        switch (selectedItem) {
            case R.id.nav_item_1:
                Intent it1 =new Intent();
                it1.setClass(Home.this, Info.class);
                startActivity(it1);
                break;
            case R.id.nav_item_2:

                Intent it2 =new Intent();
                it2.setClass(Home.this, Guild.class);
                startActivity(it2);
                break;
            case R.id.nav_item_3:
                Intent it3 =new Intent();
                it3.setClass(Home.this, Private.class);
                startActivity(it3);
                break;
            case R.id.nav_item_4:
                Intent it4 =new Intent();
                it4.setClass(Home.this, SettingPage.class);
                startActivity(it4);
                break;
            case R.id.nav_item_5:
                Intent it5 =new Intent();
                it5.setClass(Home.this, Chat.class);
                startActivity(it5);
                break;
            case R.id.nav_item_6:
                Intent it6 =new Intent();
                it6.setClass(Home.this, Pay.class);
                startActivity(it6);

                break;
            case R.id.nav_item_7:
                Toast.makeText(this, "Settings is clicked !", Toast.LENGTH_SHORT).show();
                break;
            case R.id.nav_item_8:
                Toast.makeText(this, "Settings is clicked !", Toast.LENGTH_SHORT).show();
                break;
            case R.id.nav_item_9:
                Intent it9 =new Intent();
                it9.setClass(Home.this, History.class);
                startActivity(it9);
                Toast.makeText(this, "Settings is clicked !", Toast.LENGTH_SHORT).show();
                break;
            case R.id.nav_item_10:
                Toast.makeText(this, "Settings is clicked !", Toast.LENGTH_SHORT).show();
                break;
        }

        drawerLayout.closeDrawer(GravityCompat.START);

        return true;
    }

    @Override
    public void onBackPressed() {

        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

}
